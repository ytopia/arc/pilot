module gitlab.com/youtopia.earth/ops/pilot

go 1.12

require (
	github.com/google/go-jsonnet v0.14.0
	github.com/hashicorp/consul/api v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.7
	github.com/kvz/logstreamer v0.0.0-20150507115422-a635b98146f0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/opencontainers/runc v0.1.1
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/t-tomalak/logrus-easy-formatter v0.0.0-20190827215021-c074f06c5816 // indirect
	github.com/yosuke-furukawa/json5 v0.1.1
)
