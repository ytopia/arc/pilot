package initd

import (
	"gitlab.com/youtopia.earth/ops/pilot/config"
)

type App interface {
	GetConfig() *config.Config
	GetConfigLoader() *config.ConfigLoader
}
