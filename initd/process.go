package initd

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/opencontainers/runc/libcontainer/user"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/youtopia.earth/ops/pilot/config"
	"gitlab.com/youtopia.earth/ops/pilot/decode"
	"gitlab.com/youtopia.earth/ops/pilot/discovery"
	"gitlab.com/youtopia.earth/ops/pilot/tools"
	"gitlab.com/youtopia.earth/ops/pilot/waitfor"
)

type Process struct {
	Index                          int
	Name                           string
	Port                           int
	Discovery                      bool
	Check                          []string
	Exec                           []string
	ExecTimeout                    time.Duration
	Tags                           []string
	CheckInterval                  time.Duration
	CheckTTL                       time.Duration
	CheckBeforeStart               time.Duration
	CheckRetryTimeout              time.Duration
	CheckTimeout                   time.Duration
	CheckRetryBool                 bool
	CheckRetryInt                  int
	User                           string
	Stopsignal                     syscall.Signal
	Timeout                        time.Duration
	Optional                       bool
	Dependencies                   []config.Dependency
	CleanEnv                       bool
	Env                            map[string]string
	Environ                        []string
	EnvironMap                     map[string]string
	Interfaces                     []string
	IP                             string
	DeregisterCriticalServiceAfter string
	EnableTagOverride              bool
	ProcName                       string

	ExecRunning   bool
	ExecExited    bool
	ExecExitCode  int
	ExecUser      *user.ExecUser
	Service       *discovery.ServiceDefinition
	Logger        *logrus.Entry
	Context       *context.Context
	ContextCancel *context.CancelFunc
	WaitGroup     *sync.WaitGroup
	Daemon        *Daemon
}

func NewProcess(processConfig *config.Process, index int, daemon *Daemon) *Process {
	process := &Process{}
	process.Index = index
	process.Daemon = daemon
	if err := process.Initialize(processConfig); err != nil {
		err2 := fmt.Sprintf("Error creating process %v", process.Index)
		logrus.Fatalf("%v", errors.Wrap(err, err2))
	}
	return process
}

func (process *Process) Initialize(p *config.Process) error {

	process.WaitGroup = &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	process.Context = &ctx
	process.ContextCancel = &cancel

	return process.ParseConfig(p)

}

func (process *Process) ParseConfig(p *config.Process) error {

	process.Name = p.Name

	process.Port = p.Port

	process.Optional = p.Optional

	process.Dependencies = p.Dependencies

	if process.Name != "" {
		process.ProcName = process.Name
	} else {
		process.ProcName = "process-" + strconv.Itoa(process.Index)
	}

	if err := process.ParseDiscovery(p); err != nil {
		return err
	}

	if err := process.ParseProcessDeregisterCriticalServiceAfter(p); err != nil {
		return err
	}

	if err := process.ParseProcessEnableTagOverride(p); err != nil {
		return err
	}

	if err := process.ParseInterfaces(p); err != nil {
		return err
	}

	if err := process.ParseIp(p); err != nil {
		return err
	}

	if err := process.ParseUser(p); err != nil {
		return err
	}

	if err := process.ParseEnv(p); err != nil {
		return err
	}

	if err := process.ParseCheck(p); err != nil {
		return err
	}
	if err := process.ParseExec(p); err != nil {
		return err
	}

	if err := process.ParseStopsignal(p); err != nil {
		return err
	}

	if err := process.ParseTags(p); err != nil {
		return err
	}

	if process.Discovery {
		if process.Name == "" {
			return fmt.Errorf("missing process name for service discovery: %v\n", p)
		}

		if err := process.CreateService(); err != nil {
			return err
		}
	}

	return nil
}

func (process *Process) ParseProcessDeregisterCriticalServiceAfter(p *config.Process) error {
	DeregisterCriticalServiceAfter := p.DeregisterCriticalServiceAfter
	cfg := process.Daemon.GetConfig()
	if DeregisterCriticalServiceAfter == "" {
		process.DeregisterCriticalServiceAfter = cfg.ProcessDeregisterCriticalServiceAfter
	} else {
		process.DeregisterCriticalServiceAfter = DeregisterCriticalServiceAfter
	}
	return nil
}

func (process *Process) ParseProcessEnableTagOverride(p *config.Process) error {
	EnableTagOverride := p.EnableTagOverride
	if EnableTagOverride == nil {
		cfg := process.Daemon.GetConfig()
		process.EnableTagOverride = cfg.ProcessEnableTagOverride
	} else {
		process.EnableTagOverride = EnableTagOverride.(bool)
	}
	return nil
}

func (process *Process) ParseDiscovery(p *config.Process) error {
	Discovery := p.Discovery
	if Discovery == nil {
		if process.Name != "" {
			process.Discovery = true
		} else {
			process.Discovery = false
		}
	} else {
		process.Discovery = Discovery.(bool)
	}

	return nil
}

func (process *Process) ParseEnv(p *config.Process) error {
	cfg := process.Daemon.GetConfig()

	if p.CleanEnv == nil {
		process.CleanEnv = cfg.ProcessCleanEnv
	} else {
		process.CleanEnv = *p.CleanEnv
	}

	if !process.CleanEnv {
		process.Environ = os.Environ()
	}

	if p.Env == nil {
		process.Env = cfg.ProcessEnv
	} else {
		process.Env = *p.Env
	}
	process.envHooks()
	process.EnvironMap = tools.EnvExpandMap(process.Environ, process.Env)
	process.Environ = tools.EnvToPairs(process.EnvironMap)
	return nil
}

func (process *Process) envHooks() {
	if process.IP != "" {
		envKey := process.Daemon.App.GetConfigLoader().PrefixEnv("IP")
		process.Env[envKey] = process.IP
	}

	if process.ExecUser != nil {
		process.Env["HOME"] = process.ExecUser.Home
	}
}

func (process *Process) ParseTags(p *config.Process) error {
	var err error
	process.Tags, err = decode.ToStrings(p.Tags)
	if err != nil {
		return fmt.Errorf("invalid process tags type: %T\n", p.Tags)
	}
	return nil
}

func (process *Process) ParseInterfaces(p *config.Process) error {
	cfg := process.Daemon.GetConfig()
	Interfaces := p.Interfaces
	if Interfaces == nil {
		Interfaces = cfg.ProcessInterfaces
	}
	var err error
	process.Interfaces, err = decode.ToStrings(Interfaces)
	if err != nil {
		return err
	}
	return nil
}

func (process *Process) ParseIp(p *config.Process) error {
	cfg := process.Daemon.GetConfig()
	IP := p.IP
	if IP == "" {
		IP = cfg.ProcessIP
	}

	if IP == "" {
		var err error
		IP, err = tools.GetIP(process.Interfaces)
		if err != nil {
			return err
		}
	}

	process.IP = IP

	envKey := process.getIPEnvVarName()
	os.Setenv(envKey, process.IP)

	return nil
}

func (process *Process) getIPEnvVarName() string {
	envKey := strings.ToUpper(process.Name)
	envKey = strings.Replace(envKey, "-", "_", -1)
	envKey = fmt.Sprintf("%v_IP", envKey)
	envKey = process.Daemon.App.GetConfigLoader().PrefixEnv(envKey)
	return envKey
}

func (process *Process) ParseUser(p *config.Process) error {
	cfg := process.Daemon.GetConfig()
	User := p.User
	if User == "" {
		User = cfg.ProcessUser
	}
	process.User = User

	if process.User == "" {
		return nil
	}

	defaultExecUser := user.ExecUser{
		Uid:  syscall.Getuid(),
		Gid:  syscall.Getgid(),
		Home: "/",
	}
	passwdPath, err := user.GetPasswdPath()
	if err != nil {
		return err
	}
	groupPath, err := user.GetGroupPath()
	if err != nil {
		return err
	}
	execUser, err := user.GetExecUserPath(process.User, &defaultExecUser, passwdPath, groupPath)
	if err != nil {
		return err
	}
	process.ExecUser = execUser
	return nil
}

func (process *Process) ParseStopsignal(p *config.Process) error {
	var stopsignal string
	if p.Stopsignal != "" {
		stopsignal = p.Stopsignal
	} else {
		cfg := process.Daemon.App.GetConfig()
		stopsignal = cfg.ProcessStopsignal
	}
	switch stopsignal {
	default:
		return fmt.Errorf(`unknown stopsignal "%v"`, stopsignal)
	case "SIGABRT":
		process.Stopsignal = syscall.SIGABRT
	case "SIGALRM":
		process.Stopsignal = syscall.SIGALRM
	case "SIGBUS":
		process.Stopsignal = syscall.SIGBUS
	case "SIGCHLD":
		process.Stopsignal = syscall.SIGCHLD
	case "SIGCLD":
		process.Stopsignal = syscall.SIGCLD
	case "SIGCONT":
		process.Stopsignal = syscall.SIGCONT
	case "SIGFPE":
		process.Stopsignal = syscall.SIGFPE
	case "SIGHUP":
		process.Stopsignal = syscall.SIGHUP
	case "SIGILL":
		process.Stopsignal = syscall.SIGILL
	case "SIGINT":
		process.Stopsignal = syscall.SIGINT
	case "SIGIO":
		process.Stopsignal = syscall.SIGIO
	case "SIGIOT":
		process.Stopsignal = syscall.SIGIOT
	case "SIGKILL":
		process.Stopsignal = syscall.SIGKILL
	case "SIGPIPE":
		process.Stopsignal = syscall.SIGPIPE
	case "SIGPOLL":
		process.Stopsignal = syscall.SIGPOLL
	case "SIGPROF":
		process.Stopsignal = syscall.SIGPROF
	case "SIGPWR":
		process.Stopsignal = syscall.SIGPWR
	case "SIGQUIT":
		process.Stopsignal = syscall.SIGQUIT
	case "SIGSEGV":
		process.Stopsignal = syscall.SIGSEGV
	case "SIGSTKFLT":
		process.Stopsignal = syscall.SIGSTKFLT
	case "SIGSTOP":
		process.Stopsignal = syscall.SIGSTOP
	case "SIGSYS":
		process.Stopsignal = syscall.SIGSYS
	case "SIGTERM":
		process.Stopsignal = syscall.SIGTERM
	case "SIGTRAP":
		process.Stopsignal = syscall.SIGTRAP
	case "SIGTSTP":
		process.Stopsignal = syscall.SIGTSTP
	case "SIGTTIN":
		process.Stopsignal = syscall.SIGTTIN
	case "SIGTTOU":
		process.Stopsignal = syscall.SIGTTOU
	case "SIGUNUSED":
		process.Stopsignal = syscall.SIGUNUSED
	case "SIGURG":
		process.Stopsignal = syscall.SIGURG
	case "SIGUSR1":
		process.Stopsignal = syscall.SIGUSR1
	case "SIGUSR2":
		process.Stopsignal = syscall.SIGUSR2
	case "SIGVTALRM":
		process.Stopsignal = syscall.SIGVTALRM
	case "SIGWINCH":
		process.Stopsignal = syscall.SIGWINCH
	case "SIGXCPU":
		process.Stopsignal = syscall.SIGXCPU
	case "SIGXFSZ":
		process.Stopsignal = syscall.SIGXFSZ
	}
	return nil
}

func (process *Process) ParseCheck(p *config.Process) error {
	cfg := process.Daemon.GetConfig()

	var err error

	if p.Check != nil {
		process.Check, err = decode.Exec(p.Check)
		if err != nil {
			err2 := fmt.Sprintf("invalid process check type: %T\n", p.Check)
			return errors.Wrap(err, err2)
		}
	}

	CheckIntervalInterface := p.CheckInterval
	if CheckIntervalInterface == nil {
		CheckIntervalInterface = cfg.ProcessCheckInterval
	}
	process.CheckInterval, err = decode.Duration(CheckIntervalInterface)
	if err != nil {
		err2 := fmt.Sprintf(`invalid process check-interval: "%v"`, CheckIntervalInterface)
		return errors.Wrap(err, err2)
	}

	CheckTTLInterface := p.CheckTTL
	if CheckTTLInterface == nil {
		CheckTTLInterface = cfg.ProcessCheckTTL
	}
	process.CheckTTL, err = decode.Duration(CheckTTLInterface)
	if err != nil {
		err2 := fmt.Sprintf(`invalid process check-ttl: "%v"`, CheckTTLInterface)
		return errors.Wrap(err, err2)
	}

	CheckBeforeStartInterface := p.CheckBeforeStart
	if CheckBeforeStartInterface == nil {
		CheckBeforeStartInterface = cfg.ProcessCheckBeforeStart
	}
	process.CheckBeforeStart, err = decode.Duration(CheckBeforeStartInterface)
	if err != nil {
		err2 := fmt.Sprintf(`invalid process check-before-start: "%v"`, CheckBeforeStartInterface)
		return errors.Wrap(err, err2)
	}

	CheckRetryTimeoutInterface := p.CheckRetryTimeout
	if CheckRetryTimeoutInterface == nil {
		CheckRetryTimeoutInterface = cfg.ProcessCheckRetryTimeout
	}
	process.CheckRetryTimeout, err = decode.Duration(CheckRetryTimeoutInterface)
	if err != nil {
		err2 := fmt.Sprintf(`invalid process check-retry-timeout: "%v"`, CheckRetryTimeoutInterface)
		return errors.Wrap(err, err2)
	}

	CheckTimeoutInterface := p.CheckTimeout
	if CheckTimeoutInterface == nil {
		CheckTimeoutInterface = cfg.ProcessCheckTimeout
	}
	process.CheckTimeout, err = decode.Duration(CheckTimeoutInterface)
	if err != nil {
		err2 := fmt.Sprintf(`invalid process check-timeout: "%v"`, CheckTimeoutInterface)
		return errors.Wrap(err, err2)
	}

	if err := process.ParseCheckRetry(p); err != nil {
		return err
	}

	return nil
}

func (process *Process) ParseExec(p *config.Process) error {
	cfg := process.Daemon.GetConfig()

	var err error

	process.Exec, err = decode.Exec(p.Exec)
	if err != nil {
		err2 := fmt.Sprintf("invalid process exec type: %T\n", p.Exec)
		return errors.Wrap(err, err2)
	}

	ExecTimeoutInterface := p.ExecTimeout
	if ExecTimeoutInterface == nil {
		ExecTimeoutInterface = cfg.ProcessExecTimeout
	}
	process.ExecTimeout, err = decode.Duration(ExecTimeoutInterface)
	if err != nil {
		err2 := fmt.Sprintf(`invalid process exec-timeout: "%v"`, ExecTimeoutInterface)
		return errors.Wrap(err, err2)
	}

	return nil
}

func (process *Process) ParseCheckRetry(p *config.Process) error {
	cfg := process.Daemon.GetConfig()

	CheckRetryInterface := p.CheckRetry
	if CheckRetryInterface == nil {
		CheckRetryInterface = cfg.ProcessCheckRetry
	}
	switch CheckRetryInterface.(type) {
	case int:
		process.CheckRetryInt = CheckRetryInterface.(int)
		if process.CheckRetryInt > 0 {
			process.CheckRetryBool = true
		}
	case float64:
		str := fmt.Sprintf("%f", CheckRetryInterface.(float64))
		var err error
		if process.CheckRetryInt, err = strconv.Atoi(str); err != nil {
			return err
		}
		if process.CheckRetryInt > 0 {
			process.CheckRetryBool = true
		}
	case string:
		CheckRetryString := CheckRetryInterface.(string)
		if CheckRetryString == "" {
			process.CheckRetryInt = 0
			process.CheckRetryBool = false
		} else if CheckRetryString == "1" {
			process.CheckRetryInt = 1
			process.CheckRetryBool = true
		} else {
			b, err := strconv.ParseBool(CheckRetryString)
			if err == nil {
				process.CheckRetryBool = b
			} else {
				var err error
				process.CheckRetryInt, err = strconv.Atoi(CheckRetryString)
				if err != nil {
					return fmt.Errorf(`unexpected check-retry format "%v", expected int (e.g: 10) or bool (e.g "true")`, CheckRetryString)
				}
				process.CheckRetryBool = true
			}
		}
	case nil:
	default:
		return fmt.Errorf(`invalid type for type:"%T",value:"%v"`, CheckRetryInterface, CheckRetryInterface)
	}
	return nil
}

func (process *Process) CreateService() error {
	disc := process.Daemon.GetDiscovery()

	if err := discovery.ValidateName(process.Name); err != nil {
		return err
	}

	hostname, _ := os.Hostname()
	id := fmt.Sprintf("%s-%s", process.Name, hostname)

	TTL := int(process.CheckTTL / time.Second)

	Name := process.Name
	Port := process.Port
	Tags := process.Tags

	IPAddress := process.IP
	DeregisterCriticalServiceAfter := process.DeregisterCriticalServiceAfter
	EnableTagOverride := process.EnableTagOverride

	Service := &discovery.ServiceDefinition{
		Consul:                         disc,
		ID:                             id,
		Name:                           Name,
		Port:                           Port,
		TTL:                            TTL,
		Tags:                           Tags,
		IPAddress:                      IPAddress,
		InitialStatus:                  "passing",
		DeregisterCriticalServiceAfter: DeregisterCriticalServiceAfter,
		EnableTagOverride:              EnableTagOverride,
	}

	process.Service = Service

	return nil
}

func (p *Process) Cancel() {
	(*p.ContextCancel)()
}

func (p *Process) Done() <-chan struct{} {
	return (*p.Context).Done()
}

func (process *Process) Start() error {

	daemon := process.Daemon

	process.Logger = logrus.WithFields(logrus.Fields{
		"procName": process.ProcName,
	})

	go func() {
		<-daemon.Done()
		process.Cancel()
	}()

	if len(process.Dependencies) > 0 {
		ok, err := process.waitDependencies()
		if err != nil {
			return err
		} else if !ok {
			return nil
		}
	}

	process.WaitGroup.Add(1)
	go func() {
		<-process.Done()
		if process.Discovery {
			process.Service.Deregister()
		}
		process.WaitGroup.Done()
	}()

	if len(process.Exec) > 0 {
		go process.RunExec()
	}

	if process.Discovery || len(process.Check) > 0 {
		go process.RunCheckLoop()
	}

	process.WaitGroup.Wait()

	return nil
}

func (process *Process) expandCmdEnvMapper(key string) string {
	if val, ok := process.EnvironMap[key]; ok {
		return val
	}
	return ""
}
func (process *Process) ExpandCmdEnv(commandSlice []string) []string {
	expandedCmd := make([]string, len(commandSlice))
	for i, str := range commandSlice {
		expandedCmd[i] = os.Expand(str, process.expandCmdEnvMapper)
	}
	return expandedCmd
}

func (process *Process) RunCmd(commandSlice []string, args ...interface{}) error {
	commandSlice = process.ExpandCmdEnv(commandSlice)
	globalHookFunc := func(cmd *exec.Cmd) error {
		cmd.Env = append(cmd.Env, process.Environ...)
		return nil
	}
	args = append([]interface{}{globalHookFunc}, args...)
	if process.ExecUser != nil {
		args = append(args, process.ExecUser)
	}
	return tools.RunCmd(commandSlice, args...)
}

func (process *Process) waitDependencies() (bool, error) {
	dependencies := process.Dependencies

	dependenciesJSON := tools.JsonEncode(dependencies)

	logrus.WithFields(logrus.Fields{
		"procName":     process.ProcName,
		"dependencies": dependencies,
	}).Info("loading process dependencies")
	ok, err := waitfor.Dependencies(dependenciesJSON, process.Daemon.App, process.Environ, process.Context)
	if err != nil {
		process.Logger.Errorf("process dependencies error: %v", err)
		if !process.Optional {
			process.Logger.Errorf("non optional process failed, sending stop signal to pilot")
			process.Daemon.Exit(1)
		}
	}
	return ok, err
}

func (process *Process) RunCheckLoop() {

	process.Logger.Debugf("check-before-start: %v", process.CheckBeforeStart)
	time.Sleep(process.CheckBeforeStart)

	retryStartTime := time.Now()
	errI := 0

	cfg := process.Daemon.GetConfig()
	var ready bool

checkLoop:
	for {
		ok := process.runCheck(&retryStartTime, &errI)
		if !ready && ok {
			os.MkdirAll(cfg.ReadyStateDir, os.ModePerm)
			f := cfg.ReadyStateDir + "/" + strconv.Itoa(process.Index)
			file, err := os.OpenFile(f, os.O_RDONLY|os.O_CREATE, 0644)
			if err != nil {
				logrus.Errorf("unable to file write ready state file to %v", f)
			} else {
				file.Close()
			}
			ready = true
		}
		select {
		case <-process.Done():
			break checkLoop
		default:
			process.discoveryHeartbeat()
			time.Sleep(process.CheckInterval)
		}
	}
}

func (process *Process) discoveryHeartbeat() {
	if process.Discovery {
		process.Service.SendHeartbeat()
	}
}

func (process *Process) runCheckCmd() error {
	contextFields := logrus.Fields{
		"procOp":    "check",
		"procCheck": tools.JsonEncode(process.Check),
		"procName":  process.ProcName,
	}
	var ctx = context.Background()
	if process.CheckTimeout > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, process.CheckTimeout)
		defer cancel()
	}
	err := process.RunCmd(process.Check, contextFields, ctx, logrus.DebugLevel, logrus.WarnLevel)
	if ctx.Err() == context.DeadlineExceeded {
		process.Logger.WithFields(logrus.Fields{
			"timeout": process.CheckTimeout,
		}).Warnf("process check timeout fail")
	}
	return err
}

func (process *Process) runCheckNet() error {
	port := strconv.Itoa(process.Port)
	var err error
	address := strings.Join([]string{"localhost", port}, ":")
	if process.CheckTimeout > 0 {
		_, err = net.DialTimeout("tcp", address, process.CheckTimeout)
	} else {
		_, err = net.Dial("tcp", address)
	}
	return err
}

func (process *Process) runCheck(retryStartTime *time.Time, errI *int) bool {

	var err error
	if len(process.Check) > 0 {
		err = process.runCheckCmd()
	} else if process.Port != 0 {
		err = process.runCheckNet()
	} else {
		return true
	}

	if err == nil {
		*errI = 0
		*retryStartTime = time.Now()
		return true
	}

	*errI = (*errI) + 1
	process.checkHandleError(err, errI, retryStartTime)
	return false

}

func (process *Process) checkHandleError(err error, errI *int, retryStartTime *time.Time) {
	process.Logger.Warnf("process check error: %v", err)

	var fail bool
	if !process.Optional {
		if !process.CheckRetryBool ||
			(process.CheckRetryInt != 0 && *errI >= process.CheckRetryInt) {
			process.Logger.WithFields(logrus.Fields{
				"retry": process.CheckRetryInt,
			}).Errorf("process retry fail")
			fail = true
		}

		retryTimeout := (*retryStartTime).Add(process.CheckRetryTimeout)
		if time.Now().After(retryTimeout) {
			process.Logger.WithFields(logrus.Fields{
				"retryTimeout": process.CheckRetryTimeout,
			}).Errorf("process retry-timeout fail")
			fail = true
		}
	}

	if fail {
		if process.Discovery {
			process.Service.Fail()
		}

		process.Cancel()
		logrus.Errorf("non optional process failed, sending stop signal to pilot")
		process.Daemon.Exit(1)
	} else {
		if process.Discovery {
			process.Service.Warn()
		}
	}
}

func (process *Process) RunExec() error {

	contextFields := logrus.Fields{
		"procOp":   "exec",
		"procExec": tools.JsonEncode(process.Exec),
		"procName": process.ProcName,
	}

	execTimeout := process.ExecTimeout
	var ctx context.Context
	if execTimeout > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(context.Background(), execTimeout)
		defer cancel()
	} else {
		ctx = context.Background()
	}

	process.ExecRunning = true

	process.WaitGroup.Add(1)

	ctxRunning, cancelRunning := context.WithCancel(context.Background())
	hookFunc := func(cmd *exec.Cmd) error {
		go func(cmd *exec.Cmd) {
			select {
			case <-process.Done():
				if cmd.Process != nil {
					logrus.Debugf(`sending stopsignal "%v" to process %v`, process.Stopsignal, process.Index)
					cmd.Process.Signal(process.Stopsignal)
				}
			case <-ctxRunning.Done():
				return
			}
		}(cmd)
		return nil
	}

	var err error
	err = process.RunCmd(process.Exec, contextFields, ctx, hookFunc)

	cancelRunning()
	process.WaitGroup.Done()

	if err != nil {

		process.ExecRunning = false
		process.ExecExited = true

		if exitError, ok := err.(*exec.ExitError); ok {
			process.ExecExitCode = exitError.ExitCode()
		} else {
			process.ExecExitCode = 1
		}

		if process.ExecExitCode > 0 {
			process.Logger.WithFields(logrus.Fields{
				"exitCode": process.ExecExitCode,
			}).Errorf("process exec error: %v", err)
		}

		if ctx.Err() == context.DeadlineExceeded {
			process.Logger.WithFields(logrus.Fields{
				"timeout": process.ExecTimeout,
			}).Warnf("process exec timeout fail")
		}

		process.Cancel()

		if !process.Optional && process.ExecExitCode > 0 {
			process.Logger.Errorf("non optional process failed, sending stop signal to pilot")
			process.Daemon.Exit(1)
		}

		return err
	}

	process.ExecRunning = false
	process.ExecExited = true

	return nil
}
