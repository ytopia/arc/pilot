package initd

import (
	"context"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"

	"gitlab.com/youtopia.earth/ops/pilot/config"
	"gitlab.com/youtopia.earth/ops/pilot/discovery"
	"gitlab.com/youtopia.earth/ops/pilot/sup"
	"gitlab.com/youtopia.earth/ops/pilot/tools"
	"gitlab.com/youtopia.earth/ops/pilot/waitfor"
)

type Daemon struct {
	App           App
	Context       *context.Context
	ContextCancel *context.CancelFunc
	DaemonChan    chan os.Signal
	WaitGroup     *sync.WaitGroup
	Discovery     discovery.Backend
	ExitCode      int
}

func NewDaemon(app App) *Daemon {
	daemon := &Daemon{}

	daemon.App = app

	ctx, cancel := context.WithCancel(context.Background())
	daemon.Context = &ctx
	daemon.ContextCancel = &cancel

	daemon.WaitGroup = &sync.WaitGroup{}

	daemon.InitDiscovery()

	return daemon
}

func (daemon *Daemon) GetDiscovery() discovery.Backend {
	return daemon.Discovery
}

func (daemon *Daemon) GetConfig() *config.Config {
	return daemon.App.GetConfig()
}

func (daemon *Daemon) GetWaitGroup() *sync.WaitGroup {
	return daemon.WaitGroup
}

func (daemon *Daemon) WaitShutdown() bool {
	cfg := daemon.App.GetConfig()
	return tools.WaitTimeout(daemon.WaitGroup, cfg.ShutdownTimeout)
}

func (daemon *Daemon) DaemonOpener() {
	daemon.DaemonChan = make(chan os.Signal)
	signal.Notify(daemon.DaemonChan, syscall.SIGINT, syscall.SIGTERM)
}
func (daemon *Daemon) DaemonCloser() {
	<-daemon.DaemonChan
	logrus.Info("shutdown signal received")
	daemon.Cancel()

	if daemon.WaitShutdown() {
		logrus.Info("workers done, shutting down")
	} else {
		logrus.Warn("workers timeout, shutting down")
	}
	daemon.ShutdownCleanup()
}

func (daemon *Daemon) ShutdownCleanup() {
	cfg := daemon.App.GetConfig()
	if _, err := os.Stat(cfg.ReadyStateDir); !os.IsNotExist(err) {
		logrus.Debugf("cleaning up %v", cfg.ReadyStateDir)
		if err := os.RemoveAll(cfg.ReadyStateDir + "/"); err != nil {
			logrus.Errorf("unable to remove %v", cfg.ReadyStateDir)
		}
	}
}

func (daemon *Daemon) InitDiscovery() {
	cfg := daemon.App.GetConfig()
	consul := cfg.Consul
	disc, err := discovery.NewConsul(consul)
	if err != nil {
		logrus.Fatalf("consul configuration error: %v", err)
	}
	daemon.Discovery = disc
}

func (daemon *Daemon) GetContext() *context.Context {
	return daemon.Context
}

func (daemon *Daemon) Cancel() {
	(*daemon.ContextCancel)()
}

func (daemon *Daemon) Done() <-chan struct{} {
	return (*daemon.Context).Done()
}

func (daemon *Daemon) waitforDependencies() error {

	cfg := daemon.GetConfig()

	initDir := cfg.InitDir
	initDirDependencies := []interface{}{
		map[string]string{
			"type":   "scripts-dir",
			"target": initDir,
		},
	}
	initDirDependenciesJSON := tools.JsonEncode(initDirDependencies)

	dependencies := cfg.Dependencies
	dependenciesJSON := tools.JsonEncode(dependencies)

	initDirExists, err := tools.FileExists(initDir)
	if err != nil {
		return err
	}

	environ := os.Environ()

	if initDirExists {
		logrus.WithFields(logrus.Fields{
			"init-dir": initDir,
		}).Info("loading init-dir")

		if ok, err := waitfor.Dependencies(initDirDependenciesJSON, daemon.App, environ, nil); !ok {
			return err
		}
	}

	if len(dependencies) > 0 {
		logrus.WithFields(logrus.Fields{
			"dependencies": tools.JsonEncode(dependencies),
		}).Info("loading init dependencies")

		if ok, err := waitfor.Dependencies(dependenciesJSON, daemon.App, environ, nil); !ok {
			return err
		}
	}

	return nil
}

func (daemon *Daemon) RunProcesses() error {

	cfg := daemon.GetConfig()
	wg := daemon.GetWaitGroup()

	processes := make([]*Process, len(cfg.Processes))
	for index, processConfig := range cfg.Processes {
		processes[index] = NewProcess(&processConfig, index, daemon)
	}

	for _, process := range processes {
		wg.Add(1)
		go func(process *Process) {
			defer wg.Done()
			process.Start()
		}(process)
	}

	return nil
}

func (daemon *Daemon) Run() {
	if err := daemon.RunDaemon(); err != nil {
		daemon.ExitCode = 1
	}
	os.Exit(daemon.ExitCode)
}

func (daemon *Daemon) RunDaemon() error {

	// make sure we use only a single CPU so as not to cause
	// contention on the main daemonlication
	runtime.GOMAXPROCS(1)

	// If we're running as PID1, we fork and run as a supervisor
	// so that we can cleanly handle reaping of child processes.
	// We fork before doing *anything* else so we don't have to
	// worry about where any new threads spawned by the runtime.
	if os.Getpid() == 1 {
		sup.Run() // blocks forever
		return nil
	}

	if err := daemon.waitforDependencies(); err != nil {
		return err
	}

	daemon.DaemonOpener()

	if err := daemon.RunProcesses(); err != nil {
		return err
	}

	daemon.DaemonCloser()

	return nil
}

func (daemon *Daemon) Exit(exitCode int) {
	daemon.ExitCode = exitCode
	syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
}
