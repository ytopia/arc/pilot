package waitfor

import (
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

func CheckScriptsDir(scriptsDir string, env []string, ctx *context.Context) bool {

	if _, err := os.Stat(scriptsDir); os.IsNotExist(err) {
		logrus.Debugf(`scripts-dir path "%v" doesn't exists, skipping`, scriptsDir)
		return true
	}

	var scripts []string

	err := filepath.Walk(scriptsDir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			scripts = append(scripts, path)
		}
		return nil
	})
	if err != nil {
		logrus.Fatal(err)
	}
	sort.Strings(scripts)

	hookFunc := func(cmd *exec.Cmd) error {
		cmd.Env = append(cmd.Env, env...)
		go func(cmd *exec.Cmd) {
			select {
			case <-(*ctx).Done():
				if cmd.Process != nil {
					cmd.Process.Kill()
				}
			}
		}(cmd)
		return nil
	}

	for _, script := range scripts {

		contextFields := logrus.Fields{
			"script": script,
		}
		if err := tools.RunCmd([]string{script}, contextFields, hookFunc); err != nil {
			return false
		}

	}

	return true
}
