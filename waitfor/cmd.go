package waitfor

import (
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/sirupsen/logrus"
	// "gitlab.com/youtopia.earth/ops/pilot/tools"
)

func Cmd(argv []string, app App) {
	var err error
	dependencies := argv[0]

	_, err = Dependencies(dependencies, app, os.Environ(), nil)
	if err != nil {
		os.Exit(1)
	}

	args := argv[1:]
	if len(args) > 0 {

		binary, lookErr := exec.LookPath(args[0])
		if lookErr != nil {
			logrus.Fatal(lookErr)
		}

		logrus.Info("--")
		logrus.Infof("%v", strings.Join(args, " "))

		env := os.Environ()
		execErr := syscall.Exec(binary, args, env)
		if execErr != nil {
			logrus.Fatal(execErr)
		}

	}
}
