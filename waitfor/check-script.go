package waitfor

import (
	"os/exec"
	"context"

	"github.com/sirupsen/logrus"

	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

func CheckScript(script []string, env []string, ctx *context.Context) bool {
	contextFields := logrus.Fields{
		"script": script,
	}
	hookFunc := func(cmd *exec.Cmd) error {
		cmd.Env = append(cmd.Env, env...)
		go func(cmd *exec.Cmd) {
			select {
			case <-(*ctx).Done():
				if cmd.Process != nil {
					cmd.Process.Kill()
				}
			}
		}(cmd)
		return nil
	}
	if err := tools.RunCmd(script, contextFields, hookFunc); err != nil {
		return false
	}
	return true
}
