package waitfor

import (
	"encoding/json"
	"net/http"
)

func CheckService(service string, consulHttpAddr string) bool {
	res, err := http.Get(consulHttpAddr + "/v1/health/service/" + service + "?passing")
	if err != nil {
		return false
	}
	defer res.Body.Close()

	var servicePassing interface{}
	json.NewDecoder(res.Body).Decode(&servicePassing)
	servicePassingArray := servicePassing.([]interface{})
	if len(servicePassingArray) > 0 {
		return true
	} else {
		return false
	}

}
