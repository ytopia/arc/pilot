PROJECT_NAME := "pilot"
PKG := "gitlab.com/youtopia.earth/ops/$(PROJECT_NAME)"

all: vendor fmt build targz
install: install-bin autocomplete

docker:
	stackd build
dockerfile:
	docker build -t registry.gitlab.com/youtopia.earth/ops/pilot:$${PILOT_TAG:-master} .
docker-compose:
	docker-compose build

vendor:
	go mod vendor

fmt:
	gofmt -w .

build:
	CGO_ENABLED=0 GOOS=linux go build -o pilot -v $(PKG) .

targz:
	tar -cvzf $(PROJECT_NAME).tar.gz $(PROJECT_NAME)

install-bin:
	sudo cp -f pilot /usr/local/bin/pilot

autocomplete:
	[ -f ~/.bashrc ] || touch ~/.bashrc
	grep -xq ". <(pilot completion)" ~/.bashrc || printf "\n. <(pilot completion)\n" >> ~/.bashrc


help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
