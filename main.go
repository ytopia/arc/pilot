package main

import (
	"gitlab.com/youtopia.earth/ops/pilot/app"
)

func main() {
	app.New()
}
