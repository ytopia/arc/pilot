package app

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/youtopia.earth/ops/pilot/cmd"
	"gitlab.com/youtopia.earth/ops/pilot/config"
)

type App struct {
	Config          *config.Config
	ConfigFile      *string
	ConfigEnvPrefix string
	ConfigLoader    *config.ConfigLoader
	Viper           *viper.Viper
	RootCmd         *cobra.Command
}

func New() *App {
	app := NewApp()
	app.RunCmd()
	return app
}

func NewApp() *App {
	app := &App{}

	app.ConfigEnvPrefix = "PILOT"

	var configFile string
	app.ConfigFile = &configFile

	app.ConfigLoader = config.NewConfigLoader()
	app.Config = app.ConfigLoader.Config
	app.Viper = app.ConfigLoader.Viper

	return app
}

func (app *App) GetViper() *viper.Viper {
	return app.ConfigLoader.GetViper()
}

func (app *App) GetConfig() *config.Config {
	return app.ConfigLoader.GetConfig()
}

func (app *App) GetConfigLoader() *config.ConfigLoader {
	return app.ConfigLoader
}

func (app *App) GetConfigFile() *string {
	return app.ConfigFile
}

func (app *App) GetConfigEnvPrefix() string {
	return app.ConfigEnvPrefix
}

func (app *App) RunCmd() {
	cobra.OnInitialize(app.InitConfig)

	RootCmd := cmd.NewCmd(app)
	app.RootCmd = RootCmd
	app.ConfigLoader.RootCmd = RootCmd

	if err := RootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}

func (app *App) InitConfig() {
	configLoader := app.GetConfigLoader()

	configLoader.ConfigLogFromEnv()
	configLoader.LoadDotEnv()
	configLoader.ConfigLogFromEnv()
	if len(os.Args) > 1 && (os.Args[1] == "init" || os.Args[1] == "ready") {
		configLoader.SetLoadJsonnet(true)
	}
	configLoader.SetEnvPrefix(app.ConfigEnvPrefix)
	configLoader.SetFile(app.ConfigFile)

	configLoader.Load()
}
