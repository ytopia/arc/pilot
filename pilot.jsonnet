local env = std.native('environ')();
local merge = import 'pilot-merge.jsonnet';
{
  processes: [
    {} + (
      if std.objectHas(env, 'SERVICE_NAME') && env.SERVICE_NAME != "" then {
        name: env.SERVICE_NAME
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_EXEC') && std.startsWith(env.SERVICE_EXEC, "[") then {
        exec: std.native('parseJson5')(env.SERVICE_EXEC)
      } else if std.objectHas(env, 'SERVICE_EXEC') && env.SERVICE_EXEC != "" then {
        exec: env.SERVICE_EXEC
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_PORT') && env.SERVICE_PORT != "" then {
        port: env.SERVICE_PORT
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_CHECK') && std.startsWith(env.SERVICE_CHECK, "[") then {
        check: std.native('parseJson5')(env.SERVICE_CHECK)
      } else if std.objectHas(env, 'SERVICE_CHECK') && env.SERVICE_CHECK != "" then {
        check: env.SERVICE_CHECK
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_TAGS') && std.startsWith(env.SERVICE_TAGS, "[") then {
        tags: std.native('parseJson5')(env.SERVICE_TAGS)
      } else if std.objectHas(env, 'SERVICE_TAGS') && env.SERVICE_TAGS != "" then {
        tags: env.SERVICE_TAGS
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_USER') && env.SERVICE_USER != "" then {
        user: env.SERVICE_USER
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_STOPSIGNAL') && env.SERVICE_STOPSIGNAL != "" then {
        stopsignal: env.SERVICE_STOPSIGNAL
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_INTERFACES') && std.startsWith(env.SERVICE_INTERFACES, "[") then {
        interfaces: std.native('parseJson5')(env.SERVICE_INTERFACES)
      } else if std.objectHas(env, 'SERVICE_INTERFACES') && env.SERVICE_INTERFACES != "" then {
        interfaces: env.SERVICE_INTERFACES
      } else if std.objectHas(env, 'SERVICE_INTERFACE') && std.startsWith(env.SERVICE_INTERFACE, "[") then {
        interfaces: std.native('parseJson5')(env.SERVICE_INTERFACE)
      } else if std.objectHas(env, 'SERVICE_INTERFACE') && env.SERVICE_INTERFACE != "" then {
        interfaces: env.SERVICE_INTERFACE
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_DEPENDENCIES') && (std.startsWith(env.SERVICE_DEPENDENCIES, "[") || std.startsWith(env.SERVICE_DEPENDENCIES, "{")) then {
        dependencies: std.native('parseJson5')(env.SERVICE_DEPENDENCIES)
      } else if std.objectHas(env, 'SERVICE_DEPENDENCIES') && env.SERVICE_DEPENDENCIES != "" then {
        dependencies: env.SERVICE_DEPENDENCIES
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_ENV') && std.startsWith(env.SERVICE_ENV, "{") then {
        env: std.native('parseJson5')(env.SERVICE_ENV)
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_CHECK_INTERVAL') then {
        checkInterval: env.SERVICE_CHECK_INTERVAL
      } else {}
    ) + (
      if std.objectHas(env, 'SERVICE_CHECK_TTL') then {
        checkTTL: env.SERVICE_CHECK_TTL
      } else {}
    )
  ] + merge.processes,
}