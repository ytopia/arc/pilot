package tools

import (
	"github.com/sirupsen/logrus"
)

func EnvMergeMap(args ...interface{}) map[string]string {
	envmap := make(map[string]string)
	for _, arg := range args {
		var envmerge map[string]string
		switch arg.(type) {
		case []string:
			envmerge = EnvToMap(arg.([]string))
		case map[string]string:
			envmerge = arg.(map[string]string)
		default:
			logrus.Fatalf(`invalid argument for EnvMergeMap, expected []string or map[string]string, received type:"%T",value:"%v"`, arg, arg)
		}
		for k, v := range envmerge {
			envmap[k] = v
		}
	}
	return envmap
}
