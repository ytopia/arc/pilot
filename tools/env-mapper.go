package tools

func EnvMapper(args ...interface{}) func(key string) string {
	envmap := EnvMergeMap(args...)
	envmapper := func(key string) string {
		return envmap[key]
	}
	return envmapper
}
