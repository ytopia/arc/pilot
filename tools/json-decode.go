package tools

import (
	"github.com/yosuke-furukawa/json5/encoding/json5"
)

func JsonDecode(str string) (interface{}, error) {
	var values interface{}
	if err := json5.Unmarshal([]byte(str), &values); err != nil {
		return nil, err
	}
	return values, nil
}
