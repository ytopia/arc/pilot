ARG GOLANG_VERSION=latest
ARG GOMPLATE_VERSION=v3.6.0-slim
FROM hairyhenderson/gomplate:$GOMPLATE_VERSION as gomplate
FROM golang:$GOLANG_VERSION as builder

RUN mkdir /opt/bin

# install consul-template
ARG CONSUL_TEMPLATE_VERSION
ARG CONSUL_TEMPLATE_SHA256
ENV CONSUL_TEMPLATE_VERSION=${CONSUL_TEMPLATE_VERSION:-"0.20.0"}
ENV CONSUL_TEMPLATE_SHA256=${CONSUL_TEMPLATE_SHA256}
RUN dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
  curl --retry 7 --fail -Lso /tmp/consul-template.tgz "https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_${dpkgArch}.tgz" \
  && echo $( ( [ ! -z $CONSUL_TEMPLATE_SHA256 ] && echo "$CONSUL_TEMPLATE_SHA256" ) || curl -Ls https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_SHA256SUMS | grep -e "linux_${dpkgArch}.tgz" | awk '{printf $1}') /tmp/consul-template.tgz | sha256sum \
  && tar zxf /tmp/consul-template.tgz -C /opt/bin \
  && rm /tmp/consul-template.tgz

# install micro editor
#RUN curl https://getmic.ro | bash && mv ./micro /opt/bin/
RUN go get -d github.com/zyedidia/micro/cmd/micro && \
  cd $GOPATH/src/github.com/zyedidia/micro && \
  make install && \
  mv $GOPATH/bin/micro /opt/bin/

# compile pilot
ENV GOFLAGS=-mod=vendor
WORKDIR /pilot
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o=/opt/bin/pilot .

# bash completion
RUN mkdir -p /etc/bash_completion.d && \
  printf "#!/bin/sh\n. <(pilot completion)">/etc/bash_completion.d/pilot && \
  chmod +x /etc/bash_completion.d/pilot

# create clean image
FROM scratch
COPY --from=gomplate /gomplate /usr/local/bin/
COPY --from=builder /opt/bin/ /usr/local/bin/
COPY --from=builder /etc/bash_completion.d/pilot /etc/bash_completion.d/pilot

COPY pilot.jsonnet /etc/
COPY pilot-merge.jsonnet /etc/
COPY bin/ /usr/local/bin/

CMD ["/usr/local/bin/pilot", "init"]