package config

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/joho/godotenv"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

type ConfigLoader struct {
	EnvPrefix         string
	Viper             *viper.Viper
	File              *string
	Config            *Config
	RootCmd           *cobra.Command
	enableLoadJsonnet bool
	configPaths       []string
	configName        string
	configFile        string
}

func NewConfigLoader() *ConfigLoader {
	configLoader := &ConfigLoader{}
	configLoader.EnvPrefix = "PILOT"
	configLoader.Viper = viper.New()
	configLoader.Config = NewConfig()
	configLoader.configPaths = []string{".", "/etc"}
	configLoader.configName = "pilot"
	return configLoader
}

func (configLoader *ConfigLoader) GetEnvPrefix() string {
	return configLoader.EnvPrefix
}
func (configLoader *ConfigLoader) SetEnvPrefix(envPrefix string) {
	configLoader.EnvPrefix = envPrefix
}
func (configLoader *ConfigLoader) GetViper() *viper.Viper {
	return configLoader.Viper
}
func (configLoader *ConfigLoader) SetViper(v *viper.Viper) {
	configLoader.Viper = v
}
func (configLoader *ConfigLoader) GetFile() *string {
	return configLoader.File
}
func (configLoader *ConfigLoader) SetFile(file *string) {
	configLoader.File = file
}
func (configLoader *ConfigLoader) GetConfig() *Config {
	return configLoader.Config
}
func (configLoader *ConfigLoader) SetConfig(config *Config) {
	configLoader.Config = config
}

func (configLoader *ConfigLoader) PrefixEnv(key string) string {
	return configLoader.EnvPrefix + "_" + key
}

func (configLoader *ConfigLoader) loadDotEnvFile(envfile string) {
	pwd, _ := os.Getwd()
	if ok, err := tools.FileExists(pwd + "/" + envfile); ok {
		logrus.Debugf("loading %v into env", envfile)
		godotenv.Overload(envfile)
	} else if err != nil {
		logrus.Fatal(err)
	}
}
func (configLoader *ConfigLoader) LoadDotEnv() {
	configLoader.loadDotEnvFile(".env.default")
	configLoader.loadDotEnvFile(".env")

	envKey := configLoader.PrefixEnv("ENV")
	pilotEnv := os.Getenv(envKey)
	logrus.Debugf("pilot_env: %v", pilotEnv)
	envs := strings.Split(pilotEnv, ",")
	for _, env := range envs {
		configLoader.loadDotEnvFile(".env." + env)
	}

}

func (configLoader *ConfigLoader) SetLoadJsonnet(enable bool) {
	configLoader.enableLoadJsonnet = enable
}

func (configLoader *ConfigLoader) LoadJsonnet() {
	File := *configLoader.File
	var configDirs []string
	var configName string
	if File != "" {
		dir := filepath.Dir(File)
		ext := filepath.Ext(File)
		base := filepath.Base(File)
		configDirs = []string{dir}
		configName = base[:len(base)-len(ext)]
		if ext == ".jsonnet" {
			*configLoader.File = dir + "/" + configName + ".json"
		}
	} else {
		configDirs = configLoader.configPaths
		configName = configLoader.configName
	}
	if _, err := ConfigJsonnetRender(configDirs, configName); err != nil {
		logrus.Fatal(err)
	}
}

func (configLoader *ConfigLoader) ConfigLogFromEnv() {
	rootCmd := configLoader.RootCmd

	var logType string
	logType, _ = rootCmd.PersistentFlags().GetString("log-type")
	if logType == "" {
		logType = os.Getenv(configLoader.PrefixEnv("LOG_TYPE"))
	}
	if logType != "" {
		var logForceColors bool
		if logForceColorsStr := os.Getenv(configLoader.PrefixEnv("LOG_FORCE_COLORS")); logForceColorsStr == "true" {
			logForceColors = true
		} else {
			logForceColors = false
		}
		ConfigureLogrusLogType(logType, logForceColors)
	}

	var logLevel string
	logLevel, _ = rootCmd.PersistentFlags().GetString("log-level")
	if logLevel == "" {
		logLevel = os.Getenv(configLoader.PrefixEnv("LOG_LEVEL"))
	}
	if logLevel != "" {
		ConfigureLogrusLogLevel(logLevel)
	}
}

func (configLoader *ConfigLoader) configureLogrus() {
	cfg := configLoader.Config
	ConfigureLogrusLogType(cfg.LogType, cfg.LogForceColors)
	ConfigureLogrusLogLevel(cfg.LogLevel)
}

func (configLoader *ConfigLoader) Load() {

	v := *configLoader.Viper
	v.AutomaticEnv()
	v.AllowEmptyEnv(false)
	replacer := strings.NewReplacer("-", "_", ".", "_")
	v.SetEnvKeyReplacer(replacer)
	v.SetEnvPrefix(configLoader.EnvPrefix)

	if configLoader.enableLoadJsonnet {
		configLoader.LoadJsonnet()
	}

	File := *configLoader.File
	if File != "" {
		v.SetConfigFile(File)
	} else {
		for _, configPath := range configLoader.configPaths {
			v.AddConfigPath(configPath)
		}
		v.SetConfigName(configLoader.configName)
	}

	cfg := configLoader.Config

	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			logrus.Fatalf("Unable to read config: %v", err)
		}
	}

	opt := viper.DecodeHook(mapstructure.ComposeDecodeHookFunc(
		DecodeHookParseDuration(),
		DecodeHookJsonStringAutoDecode(*cfg),
	))
	if err := v.Unmarshal(cfg, opt); err != nil {
		logrus.Fatalf("Unable to unmarshal config: %v", err)
	}

	configLoader.configureLogrus()

	configLoader.configureVarConsul()

}
