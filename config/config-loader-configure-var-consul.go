package config

import (
	"net"
	"os"
	// "github.com/sirupsen/logrus"
)

func (configLoader *ConfigLoader) configureVarConsul() {
	cfg := configLoader.Config
	_, port, _ := net.SplitHostPort(cfg.Consul)
	if port == "" {
		cfg.Consul += ":8500"
		configLoader.Viper.Set("CONSUL", cfg.Consul)
	}
	os.Setenv(configLoader.PrefixEnv("CONSUL"), cfg.Consul)
}
