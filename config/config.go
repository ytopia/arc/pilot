package config

import (
	"time"
)

func NewConfig() *Config {
	config := &Config{}
	return config
}

type Config struct {
	Consul                                string            `mapstructure:"CONSUL" json:"consul,omitempty"`
	ConsulHTTPS                           bool              `mapstructure:"CONSUL_HTTPS" json:"consulHTTPS,omitempty"`
	InitDir                               string            `mapstructure:"INIT_DIR" json:"initDir,omitempty"`
	Dependencies                          []Dependency      `mapstructure:"DEPENDENCIES" json:"dependencies,omitempty"`
	User                                  string            `mapstructure:"USER" json:"user,omitempty"`
	LogLevel                              string            `mapstructure:"LOG_LEVEL" json:"logLevel,omitempty"`
	LogType                               string            `mapstructure:"LOG_TYPE" json:"logType,omitempty"`
	LogForceColors                        bool              `mapstructure:"LOG_FORCE_COLORS" json:"logForceColors,omitempty"`
	ProcessDiscovery                      bool              `mapstructure:"PROCESS_DISCOVERY" json:"processDiscovery,omitempty"`
	ProcessCheckBeforeStart               interface{}       `mapstructure:"PROCESS_CHECK_BEFORE_START" json:"processCheckBeforeStart,omitempty"`
	ProcessCheckInterval                  interface{}       `mapstructure:"PROCESS_CHECK_INTERVAL" json:"processCheckInterval,omitempty"`
	ProcessCheckTTL                       interface{}       `mapstructure:"PROCESS_CHECK_TTL" json:"processCheckTTL,omitempty"`
	ProcessCheckTimeout                   interface{}       `mapstructure:"PROCESS_CHECK_TIMEOUT" json:"processCheckTimeout,omitempty"`
	ProcessCheckRetry                     interface{}       `mapstructure:"PROCESS_CHECK_RETRY" json:"processCheckRetry,omitempty"`
	ProcessCheckRetryTimeout              interface{}       `mapstructure:"PROCESS_CHECK_RETRY_TIMEOUT" json:"ProcessCheckRetryTimeout,omitempty"`
	ProcessExecTimeout                    interface{}       `mapstructure:"PROCESS_EXEC_TIMEOUT" json:"processExecTimeout,omitempty"`
	ProcessDependencies                   []Dependency      `mapstructure:"PROCESS_DEPENDENCIES" json:"processDependencies,omitempty"`
	ProcessUser                           string            `mapstructure:"PROCESS_USER" json:"processUser,omitempty"`
	ProcessCleanEnv                       bool              `mapstructure:"PROCESS_CLEAN_ENV" json:"processCleanEnv,omitempty"`
	ProcessEnv                            map[string]string `mapstructure:"PROCESS_ENV" json:"processEnv,omitempty"`
	ProcessInterfaces                     []string          `mapstructure:"PROCESS_INTERFACES" json:"processInterfaces,omitempty"`
	ProcessIP                             string            `mapstructure:"PROCESS_IP" json:"processIP,omitempty"`
	ProcessDeregisterCriticalServiceAfter string            `mapstructure:"PROCESS_DEREGISTER_CRITICAL_SERVICE_AFTER" json:"processDeregisterCriticalServiceAfter,omitempty"`
	ProcessEnableTagOverride              bool              `mapstructure:"PROCESS_ENABLE_TAG_OVERRIDE" json:"processEnableTagOverride,omitempty"`
	ProcessStopsignal                     string            `mapstructure:"PROCESS_STOPSIGNAL" json:"processStopsignal,omitempty"`
	Processes                             []Process         `mapstructure:"PROCESSES" json:"processes,omitempty"`
	DependencyRetry                       string            `mapstructure:"DEPENDENCY_RETRY" json:"dependencyRetry,omitempty"`
	DependencyTimeout                     time.Duration     `mapstructure:"DEPENDENCY_TIMEOUT" json:"dependencyTimeout,omitempty"`
	DependencyRetryDelay                  time.Duration     `mapstructure:"DEPENDENCY_RETRY_DELAY" json:"dependencyRetryDelay,omitempty"`
	DependencyCleanEnv                    bool              `mapstructure:"DEPENDENCY_CLEAN_ENV" json:"dependencyCleanEnv,omitempty"`
	DependencyEnv                         map[string]string `mapstructure:"DEPENDENCY_ENV" json:"dependencyEnv,omitempty"`
	ShutdownTimeout                       time.Duration     `mapstructure:"SHUTDOWN_TIMEOUT" json:"shutdownTimeout,omitempty"`
	ReadyStateDir                         string            `mapstructure:"READY_STATE_DIR" json:"readyStateDir,omitempty"`
}
