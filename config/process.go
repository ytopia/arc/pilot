package config

type Process struct {
	Name                           string
	Port                           int
	Exec                           interface{}
	ExecTimeout                    interface{}
	Tags                           interface{}
	Check                          interface{}
	CheckInterval                  interface{}
	CheckTTL                       interface{}
	CheckBeforeStart               interface{}
	CheckRetry                     interface{}
	CheckRetryTimeout              interface{}
	CheckTimeout                   interface{}
	User                           string
	Optional                       bool
	Discovery                      interface{}
	Dependencies                   []Dependency
	CleanEnv                       *bool
	Env                            *map[string]string
	Interfaces                     []string
	IP                             string
	Stopsignal                     string
	DeregisterCriticalServiceAfter string
	EnableTagOverride              interface{}
}
