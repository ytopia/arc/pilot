package xjsonnet

import (
	jsonnet "github.com/google/go-jsonnet"
	"github.com/google/go-jsonnet/ast"
	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

func NativeFunctionParseJson5() *jsonnet.NativeFunction {
	var nativeFunctionParseJson5 = &jsonnet.NativeFunction{
		Name:   "parseJson5",
		Params: ast.Identifiers{"x"},
		Func: func(x []interface{}) (interface{}, error) {
			json, err := tools.JsonDecode(x[0].(string))
			if err != nil {
				return nil, err
			}
			return json, nil
		},
	}
	return nativeFunctionParseJson5
}
