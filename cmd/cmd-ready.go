package cmd

import (
	"os"
	"strconv"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

func CmdReady(app App) *cobra.Command {

	var cmd = &cobra.Command{
		Use:   "ready [PROCESS...]",
		Short: "check process is ready",
		Run: func(cobraCmd *cobra.Command, argv []string) {
			cfg := app.GetConfig()

			var processes []string
			if len(argv) == 0 {
				for index, _ := range cfg.Processes {
					processes = append(processes, strconv.Itoa(index))
				}
			} else {
				for _, arg := range argv {
					if index, err := strconv.Atoi(arg); err == nil {
						if index > len(cfg.Processes)-1 {
							logrus.Fatalf("process index %v not found", arg)
						}
						processes = append(processes, arg)
					} else {
						found := false
						for index, p := range cfg.Processes {
							if arg == p.Name {
								processes = append(processes, strconv.Itoa(index))
								found = true
								break
							}
						}
						if !found {
							logrus.Fatalf("process name %v not found", arg)
						}
					}
				}
			}

			var ready = true

			for _, p := range processes {
				f := cfg.ReadyStateDir + "/" + p
				if ok, _ := tools.FileExists(f); !ok {
					ready = false
					break
				}
			}

			if !ready {
				os.Exit(1)
			}

		},
	}

	cmd.Flags().String("ready-state-dir", os.TempDir()+"/pilot/ready-state", "ready state dir")
	viper.BindPFlag("READY_STATE_DIR", cmd.Flags().Lookup("ready-state-dir"))

	return cmd

}
