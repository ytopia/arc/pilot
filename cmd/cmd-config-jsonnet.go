package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/pilot/config"
)

func CmdConfigJsonnet(app App) *cobra.Command {

	var cmd = &cobra.Command{
		Use:   "jsonnet name",
		Short: "Render a jsonnet file to json",
		Args:  cobra.ExactArgs(1),
		Run: func(cobraCmd *cobra.Command, argv []string) {
			file := argv[0]
			dirpaths := []string{"."}
			if _, err := config.ConfigJsonnetRender(dirpaths, file); err != nil {
				logrus.Fatal(err)
			}
		},
	}

	return cmd

}
