package cmd

import (
	"fmt"
	"regexp"
	"sort"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

func CmdEnv(app App) *cobra.Command {
	var envCmd = &cobra.Command{
		Use:     "env",
		Short:   "Export environment variables",
		Long:    `Export environment variables`,
		Example: `eval $(pilot env)`,
		Run: func(cobraCmd *cobra.Command, args []string) {
			reNl := regexp.MustCompile(`\r?\n`)
			reQ := regexp.MustCompile(`"`)

			configInterface, err := tools.JsonDecode(tools.JsonEncode(app.GetConfig()))
			config := configInterface.(map[string]interface{})
			if err != nil {
				logrus.Fatal(err)
			}

			keys := make([]string, len(config))
			i := 0
			for k := range config {
				keys[i] = k
				i++
			}
			sort.Strings(keys)

			configLoader := app.GetConfigLoader()

			for _, key := range keys {
				exportKey := strings.ToUpper(key)
				exportValue := config[key]
				switch exportValue.(type) {
				case []interface{}:
					exportValue = tools.JsonEncode(&exportValue)
				case map[string]interface{}:
					exportValue = tools.JsonEncode(exportValue.(map[string]interface{}))
				case string:
					exportValue = reNl.ReplaceAllString(exportValue.(string), " ")
				case nil:
					exportValue = ""
				default:
					exportValue = fmt.Sprintf("%v", exportValue)
				}
				exportValue = reQ.ReplaceAllString(exportValue.(string), `\"`)
				fmt.Printf("export "+configLoader.PrefixEnv(exportKey)+`="%v"`+"\n", exportValue)
			}
		},
	}
	return envCmd
}
