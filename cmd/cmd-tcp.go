package cmd

import (
	"net"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

func CmdTcp(app App) *cobra.Command {

	tcpCmd := &cobra.Command{
		Use:   "tcp",
		Short: "Check tcp connection",
		Long:  "Check if tcp connection is available on [host:]port",
		Args:  cobra.RangeArgs(1, 2),
		Run: func(cobraCmd *cobra.Command, argv []string) {
			var host string
			var port string

			if len(argv) == 2 {
				host = argv[0]
				port = argv[1]
			} else {
				host = "localhost"
				port = argv[0]
			}

			_, err := net.Dial("tcp", strings.Join([]string{host, port}, ":"))
			if err != nil {
				os.Exit(1)
			}
		},
	}

	return tcpCmd
}
