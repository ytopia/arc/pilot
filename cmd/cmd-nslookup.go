package cmd

import (
	"fmt"
	"net"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func CmdNslookup(app App) *cobra.Command {

	nslookupCmd := &cobra.Command{
		Use:   "nslookup",
		Short: "Get IP for domain name",
		Args:  cobra.ExactArgs(1),
		Run: func(cobraCmd *cobra.Command, argv []string) {
			name := argv[0]
			ipList, err := net.LookupHost(name)
			if err != nil {
				logrus.Fatal(err)
			}
			ipListStr := strings.Join(ipList, " ")
			fmt.Println(ipListStr)
		},
	}

	return nslookupCmd
}
