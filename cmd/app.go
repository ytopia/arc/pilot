package cmd

import (
	"github.com/spf13/viper"
	"gitlab.com/youtopia.earth/ops/pilot/config"
)

type App interface {
	GetConfig() *config.Config
	GetViper() *viper.Viper
	GetConfigLoader() *config.ConfigLoader
	GetConfigFile() *string
}
