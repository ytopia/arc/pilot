package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

func NewCmd(app App) *cobra.Command {
	cmd := CmdRoot(app)

	cmd.AddCommand(CmdEnv(app))
	cmd.AddCommand(CmdInit(app))
	cmd.AddCommand(CmdTcp(app))
	cmd.AddCommand(CmdWaitfor(app))
	cmd.AddCommand(CmdConfigJsonnet(app))
	cmd.AddCommand(CmdNslookup(app))
	cmd.AddCommand(CmdIp(app))
	cmd.AddCommand(CmdReady(app))
	cmd.AddCommand(CmdCompletion(app, cmd))

	return cmd
}

func CmdRoot(app App) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "pilot",
		Short: "Container on Autopilot",
		Long:  "Container on Autopilot pattern (inspired by containerpilot)",
	}

	configFile := app.GetConfigFile()
	viper := app.GetViper()

	cmd.PersistentFlags().StringVarP(configFile, "config", "c", os.Getenv("PILOT_CONFIG"), "pilot config file (default is pilot.yml in . or /etc)")
	viper.BindPFlag("CONFIG", cmd.PersistentFlags().Lookup("config"))

	cmd.PersistentFlags().StringP("log-level", "l", "info", "log level panic|fatal|error|warning|info|debug|trace")
	viper.BindPFlag("LOG_LEVEL", cmd.PersistentFlags().Lookup("log-level"))

	cmd.PersistentFlags().StringP("log-type", "f", "json", "log type json|text")
	viper.BindPFlag("LOG_TYPE", cmd.PersistentFlags().Lookup("log-type"))

	cmd.PersistentFlags().BoolP("log-force-colors", "x", false, "force log colors for text log when no tty")
	viper.BindPFlag("LOG_FORCE_COLORS", cmd.PersistentFlags().Lookup("log-force-colors"))

	return cmd
}
