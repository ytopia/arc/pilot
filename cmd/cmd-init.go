package cmd

import (
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/youtopia.earth/ops/pilot/initd"
)

func CmdInit(app App) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "init",
		Short: "Init system",
		Long:  "Container init system with service discovery",
		Run: func(cobraCmd *cobra.Command, argv []string) {
			daemon := initd.NewDaemon(app)
			daemon.Run()
		},
	}

	viper := app.GetViper()

	cmd.Flags().String("init-dir", "/etc/pilot-init.d", "pilot directory containing autoloaded init scripts")
	viper.BindPFlag("INIT_DIR", cmd.Flags().Lookup("init-dir"))

	cmd.Flags().String("dependencies", "[]", "dependencies to wait for before running processes")
	viper.BindPFlag("DEPENDENCIES", cmd.Flags().Lookup("dependencies"))

	cmd.Flags().String("shutdown-timeout", "5", "shutdown timeout")
	viper.BindPFlag("SHUTDOWN_TIMEOUT", cmd.Flags().Lookup("shutdown-timeout"))

	cmd.Flags().String("consul", "consul:8500", "consul address")
	viper.BindPFlag("CONSUL", cmd.Flags().Lookup("consul"))

	cmd.Flags().Bool("consul-https", false, "external consul http api enable https for dependencies and waitfor")
	viper.BindPFlag("CONSUL_HTTPS", cmd.Flags().Lookup("consul-https"))

	cmd.Flags().String("user", "", "default process user")
	viper.BindPFlag("USER", cmd.Flags().Lookup("user"))

	cmd.Flags().String("process-check", `pilot-nc localhost {{ env "PILOT_PROCESS_PORT" }}`, "default check for main process")
	viper.BindPFlag("PROCESS_CHECK", cmd.Flags().Lookup("process-check"))

	cmd.Flags().String("process-check-interval", "15", "default global check interval")
	viper.BindPFlag("PROCESS_CHECK_INTERVAL", cmd.Flags().Lookup("process-check-interval"))

	cmd.Flags().String("process-check-ttl", "30", "default global check ttl")
	viper.BindPFlag("PROCESS_CHECK_TTL", cmd.Flags().Lookup("process-check-ttl"))

	cmd.Flags().String("process-check-before-start", "5", "default global wait time before start check")
	viper.BindPFlag("PROCESS_CHECK_BEFORE_START", cmd.Flags().Lookup("process-check-before-start"))

	cmd.Flags().String("process-check-retry", "true", "default global retry attempts (true for no limit)")
	viper.BindPFlag("PROCESS_CHECK_RETRY", cmd.Flags().Lookup("process-check-retry"))

	cmd.Flags().String("process-check-retry-timeout", "120", "default global retry timeout")
	viper.BindPFlag("PROCESS_CHECK_RETRY_TIMEOUT", cmd.Flags().Lookup("process-check-retry-timeout"))

	cmd.Flags().String("process-retry-timeout", "15", "default global check timeout")
	viper.BindPFlag("PROCESS_CHECK_TIMEOUT", cmd.Flags().Lookup("process-retry-timeout"))

	cmd.Flags().String("process-exec-timeout", "0", "default global exec timeout")
	viper.BindPFlag("PROCESS_EXEC_TIMEOUT", cmd.Flags().Lookup("process-exec-timeout"))

	cmd.Flags().Bool("process-discovery", true, "enable process discovery by registering on consul")
	viper.BindPFlag("PROCESS_DISCOVERY", cmd.Flags().Lookup("process-discovery"))

	cmd.Flags().Bool("process-clean-env", false, "process run without actual env vars")
	viper.BindPFlag("PROCESS_CLEAN_ENV", cmd.Flags().Lookup("process-clean-env"))

	cmd.Flags().String("process-env", "{}", "process env vars map in json5 format")
	viper.BindPFlag("PROCESS_ENV", cmd.Flags().Lookup("process-env"))

	cmd.Flags().String("process-interfaces", "[]", "interfaces in json5 format")
	viper.BindPFlag("PROCESS_INTERFACES", cmd.Flags().Lookup("process-interfaces"))

	cmd.Flags().String("process-ip", "", "ip address")
	viper.BindPFlag("PROCESS_IP", cmd.Flags().Lookup("process-ip"))

	cmd.Flags().Bool("process-enable-tag-override", false, "consul enableTagOverride")
	viper.BindPFlag("PROCESS_ENABLE_TAG_OVERRIDE", cmd.Flags().Lookup("process-enable-tag-override"))

	cmd.Flags().String("process-deregister-critical-service-after", "24h", "consul deregisterCriticalServiceAfter")
	viper.BindPFlag("PROCESS_DEREGISTER_CRITICAL_SERVICE_AFTER", cmd.Flags().Lookup("process-deregister-critical-service-after"))

	cmd.Flags().String("process-stopsignal", "SIGTERM", "default signal to send to process when pilot init stop")
	viper.BindPFlag("PROCESS_STOPSIGNAL", cmd.Flags().Lookup("process-stopsignal"))

	cmd.Flags().StringP("retry", "r", "true", "default retry for dependencies as number or true for no limit")
	viper.BindPFlag("DEPENDENCY_RETRY", cmd.Flags().Lookup("retry"))

	cmd.Flags().StringP("timeout", "t", "5m", "default retry timeout for dependencies")
	viper.BindPFlag("DEPENDENCY_TIMEOUT", cmd.Flags().Lookup("timeout"))

	cmd.Flags().StringP("delay", "d", "2s", "default retry delay for dependencies")
	viper.BindPFlag("DEPENDENCY_RETRY_DELAY", cmd.Flags().Lookup("delay"))

	cmd.Flags().String("ready-state-dir", os.TempDir()+"/pilot/ready-state", "ready state dir")
	viper.BindPFlag("READY_STATE_DIR", cmd.Flags().Lookup("ready-state-dir"))

	return cmd
}
