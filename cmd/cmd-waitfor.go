package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/youtopia.earth/ops/pilot/waitfor"
)

func CmdWaitfor(app App) *cobra.Command {

	var cmd = &cobra.Command{
		Use:   "waitfor [flags] consul:8500 [--] [command]",
		Short: "Dependencies manager for container autopilot pattern",
		Long:  "Wait for a list of dependencies: service (consul), a tcp (host:port), http (url code), script or scripts-dir to run a program or exit",
		Args:  cobra.MinimumNArgs(1),
		Example: `
# service (consul service name):
  pilot waitfor service-name
  #is equivalent to:
    pilot waitfor '{type:"service",target:"service-name"}'

# tcp:
  pilot waitfor hostname:80
  #is equivalent to:
    pilot waitfor '{type:"tcp",target:"hostname:80"}'

# http:
  pilot waitfor https://gitlab.com
  # is equivalent to:
    pilot waitfor '{type:"http",target:"https://gitlab.com"}'
    # with ports range:
    pilot waitfor '{type:"http",target:["https://gitlab.com","200-299"]}'

  # with expected response codes
  pilot waitfor '[["https://gitlab.com",200,301]]'
  # is equivalent to:
    pilot waitfor '[{type:"http",target:["https://gitlab.com",200,301]}]'

  # with unexpected response codes
  pilot waitfor '[["https://gitlab.com","!500"]]'
  # is equivalent to:
    pilot waitfor '[{type:"http",target:["https://gitlab.com","!500"]}]'

# script:
  pilot waitfor /absolute/path/to/my/script
  # is equivalent to:
    pilot waitfor '{type:"script",target:"/absolute/path/to/my/script"}'

  # with arguments:
  pilot waitfor '{type:"script",target:["/absolute/path/to/my/script","arg1","arg2"]}'
  #with relative path:
  pilot waitfor '{type:"script",target:["relative/path/to/my/script","arg1","arg2"]}'

# scripts-dir:
  pilot waitfor /path/to/my/scripts-dir/
  # is equivalent to:
    pilot waitfor waitfor '{type:"scripts-dir",target:"/path/to/my/scripts-dir"}'

# combine them:
waitfor '["service-name","https://giltab.com",{type:"script",target:["my-script","arg"]}]'

`,
		Run: func(cobraCmd *cobra.Command, argv []string) {
			waitfor.Cmd(argv, app)
		},
	}

	cmd.Flags().StringP("retry", "r", "true", "default retry for dependencies as number or true for no limit")
	viper.BindPFlag("DEPENDENCY_RETRY", cmd.Flags().Lookup("retry"))

	cmd.Flags().StringP("timeout", "t", "20m", "default retry timeout for dependencies")
	viper.BindPFlag("DEPENDENCY_TIMEOUT", cmd.Flags().Lookup("timeout"))

	cmd.Flags().StringP("delay", "d", "2s", "default retry delay for dependencies")
	viper.BindPFlag("DEPENDENCY_RETRY_DELAY", cmd.Flags().Lookup("delay"))

	cmd.Flags().String("consul", "consul:8500", "consul address")
	viper.BindPFlag("CONSUL", cmd.Flags().Lookup("consul"))

	cmd.Flags().Bool("consul-https", false, "external consul http api enable https for dependencies and waitfor")
	viper.BindPFlag("CONSUL_HTTPS", cmd.Flags().Lookup("consul-https"))

	return cmd

}
