package cmd

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/youtopia.earth/ops/pilot/decode"
	"gitlab.com/youtopia.earth/ops/pilot/tools"
)

func CmdIp(app App) *cobra.Command {

	ipCmd := &cobra.Command{
		Use:   "ip [interfaces...]",
		Short: "Get IP for interface(s)",
		Run: func(cobraCmd *cobra.Command, argv []string) {
			var interfaces []string
			for _, arg := range argv {
				if arg[0:1] == "[" {
					json, err := tools.JsonDecode(arg)
					if err != nil {
						logrus.Fatal(err)
					}
					ifaces, err := decode.ToStrings(json)
					if err != nil {
						logrus.Fatal(err)
					}
					for _, iface := range ifaces {
						interfaces = append(interfaces, iface)
					}
				} else {
					interfaces = append(interfaces, arg)
				}
			}
			ip, err := tools.GetIP(interfaces)
			if err != nil {
				logrus.Fatal(err)
			}
			fmt.Println(ip)
		},
	}

	return ipCmd
}
